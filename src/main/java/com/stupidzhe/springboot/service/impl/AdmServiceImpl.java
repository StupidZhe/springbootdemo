package com.stupidzhe.springboot.service.impl;

import com.stupidzhe.springboot.domain.AdmRecord;
import com.stupidzhe.springboot.service.AdmService;
import org.springframework.stereotype.Service;

/**
 * Created by Mr.W on 2017/5/10.
 * 逻辑层
 */
@Service
public class AdmServiceImpl extends BaseServiceImpl implements AdmService {

    @Override
    public AdmRecord getAdm(Integer admId) {
        AdmRecord admRecord;
        //ValueOperations<String, AdmRecord> operations = redisTemplate.opsForValue();
        //if(redisTemplate.hasKey("a")) {
        admRecord = admCache.getAdmCache(admId);
        if (null != admRecord){
            //admRecord = operations.get("a");
            System.out.println("get a in the storage");
        } else {
            admRecord = admRecordMapper.selectByPrimaryKey(admId);
            //operations.set("a", admRecord, 3, TimeUnit.SECONDS);
            admCache.setAdmCache(admRecord);
            System.out.println("get a in the mysql");
        }
        return admRecord;
    }

    @Override
    public boolean delAdm(Integer admId) {
        //if(redisTemplate.hasKey("a")) {
        AdmRecord admRecord = admCache.getAdmCache(admId);
        if (null != admRecord){
            admCache.delRecord(admId);
            System.out.println("delete a in the storage");
            return true;
        } else {
            return false;
        }
    }
}
