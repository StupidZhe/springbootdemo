package com.stupidzhe.springboot.service.impl;

import com.stupidzhe.springboot.cache.AdmCache;
import com.stupidzhe.springboot.dao.AdmRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by Mr.W on 2017/5/11.
 */
@Service
public class BaseServiceImpl {

    @Autowired
    protected RedisTemplate redisTemplate;

    @Autowired
    protected AdmCache admCache;

    @Autowired
    protected AdmRecordMapper admRecordMapper;

}
