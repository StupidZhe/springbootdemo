package com.stupidzhe.springboot.service;

import com.stupidzhe.springboot.domain.AdmRecord;

/**
 * Created by Mr.W on 2017/5/10.
 */
public interface AdmService {

    /**
     * 获取管理员信息
     * @return admRecord
     */
    AdmRecord getAdm(Integer admId);



    /**
     * 删除管理员缓存
     * @return bool
     */
    boolean delAdm(Integer admId);
}
