package com.stupidzhe.springboot.controller.base;

import com.stupidzhe.springboot.service.AdmService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Mr.W on 2017/5/11.
 * 基本控制器
 */
public class BaseController {

    @Autowired
    protected AdmService admService;

}
