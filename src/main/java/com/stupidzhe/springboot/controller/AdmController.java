package com.stupidzhe.springboot.controller;

import com.stupidzhe.springboot.controller.base.BaseController;
import com.stupidzhe.springboot.domain.AdmRecord;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mr.W on 2017/5/11.
 * RESTful
 */
@RestController
public class AdmController extends BaseController {
    @RequestMapping(value = "/adm/{admId}", method = RequestMethod.GET)
    public AdmRecord index(@PathVariable("admId")Integer admId) {
        return admService.getAdm(admId);
    }

    @RequestMapping(value = "/adm/{admId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("admId")Integer admId) {
        admService.delAdm(admId);
    }
}
