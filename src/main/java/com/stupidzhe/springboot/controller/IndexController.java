package com.stupidzhe.springboot.controller;

import com.stupidzhe.springboot.controller.base.BaseController;
import com.stupidzhe.springboot.inter.StupidInterface;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Mr.W on 2017/5/10.
 * indexController
 */
@Controller
@StupidInterface(is = StupidInterface.Attr.SMART)
public class IndexController extends BaseController {
    private String age = "1";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        model.addAttribute("pigger", "&lt;script&gtalert('1');&lt;/script&gt");

            Method method = getClass().getMethod("index");
            method.invoke(getClass(), null);
            //String priv        = Modifier.toString(mo);
            //System.out.println(priv);

//        if (fields[0].isAnnotationPresent(StupidInterface.class)) {
//            System.out.println("1");
//        } else if (fields[0].isAnnotationPresent(Controller.class)) {
//            System.out.println("2");
//        }
        return "adm/index";
    }

}