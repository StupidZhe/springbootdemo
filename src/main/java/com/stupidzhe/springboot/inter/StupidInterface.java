package com.stupidzhe.springboot.inter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Mr.W on 2017/5/11.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface StupidInterface {
    public enum Attr{ COOL,SMART,WARM};
    Attr is() default Attr.COOL;
}

