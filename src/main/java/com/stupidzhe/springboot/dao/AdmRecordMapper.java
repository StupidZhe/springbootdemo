package com.stupidzhe.springboot.dao;

import com.stupidzhe.springboot.domain.AdmRecord;
import com.stupidzhe.springboot.domain.AdmRecordExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdmRecordMapper {
    long countByExample(AdmRecordExample example);

    int deleteByExample(AdmRecordExample example);

    int deleteByPrimaryKey(Integer admId);

    int insert(AdmRecord record);

    int insertSelective(AdmRecord record);

    List<AdmRecord> selectByExample(AdmRecordExample example);

    AdmRecord selectByPrimaryKey(Integer admId);

    int updateByExampleSelective(@Param("record") AdmRecord record, @Param("example") AdmRecordExample example);

    int updateByExample(@Param("record") AdmRecord record, @Param("example") AdmRecordExample example);

    int updateByPrimaryKeySelective(AdmRecord record);

    int updateByPrimaryKey(AdmRecord record);
}