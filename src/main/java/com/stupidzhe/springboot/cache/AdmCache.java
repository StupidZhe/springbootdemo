package com.stupidzhe.springboot.cache;

import com.stupidzhe.springboot.constant.CacheConst;
import com.stupidzhe.springboot.domain.AdmRecord;
import org.springframework.stereotype.Component;

/**
 * Created by Mr.W on 2017/5/11.
 */

@Component
public class AdmCache extends BaseCache {

    /**
     * 放入内存
     * @param admCache 需序列化对象
     */
    public void setAdmCache(AdmRecord admCache) {
        setCache(CacheConst.ADM_CACHE, admCache.getAdmId(), admCache);
    }

    /**
     * 获取
     * @param admId 编号
     * @return
     */
    public AdmRecord getAdmCache(Integer admId) {
        return null != getCache(CacheConst.ADM_CACHE, admId)?(AdmRecord) getCache(CacheConst.ADM_CACHE, admId):null;
    }

    /**
     * 删除
     * @param admId 编号
     */
    public void delRecord(Integer admId) {
        delCache(CacheConst.ADM_CACHE, admId);
    }
}
