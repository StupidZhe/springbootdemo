package com.stupidzhe.springboot.cache;

/**
 * Created by Mr.W on 2017/5/11.
 * J2cache工具
 */

import net.oschina.j2cache.CacheChannel;
import net.oschina.j2cache.J2Cache;
import org.springframework.stereotype.Component;

@Component
class BaseCache {

    /**
     * j2cache渠道
     */
    private CacheChannel cacheChannel = J2Cache.getChannel();

    /**
     * 放入内存
     * @param cacheName redis通道
     * @param key       键
     * @param val       值
     */
    void setCache(String cacheName, Object key, Object val) {
        cacheChannel.set(cacheName, key, val);
    }

    /**
     * 抓取对象
     * @param cacheName redis通道
     * @param key       键
     * @return          对象
     */
    Object getCache(String cacheName, Object key) {
        return null != cacheChannel.get(cacheName, key)? cacheChannel.get(cacheName, key).getValue() : null;
    }

    /**
     * 清除对象
     * @param cacheName redis通道
     * @param key       键
     */
    void delCache(String cacheName, Object key) {
        cacheChannel.evict(cacheName, key);
    }

    /**
     * 清空通道
     * @param cacheName redis通道
     */
    void delCannel(String cacheName) {
        cacheChannel.clear(cacheName);
    }

}
