package com.stupidzhe.springboot.domain;

import java.util.ArrayList;
import java.util.List;

public class AdmRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public AdmRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAdmIdIsNull() {
            addCriterion("adm_id is null");
            return (Criteria) this;
        }

        public Criteria andAdmIdIsNotNull() {
            addCriterion("adm_id is not null");
            return (Criteria) this;
        }

        public Criteria andAdmIdEqualTo(Integer value) {
            addCriterion("adm_id =", value, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdNotEqualTo(Integer value) {
            addCriterion("adm_id <>", value, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdGreaterThan(Integer value) {
            addCriterion("adm_id >", value, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("adm_id >=", value, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdLessThan(Integer value) {
            addCriterion("adm_id <", value, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdLessThanOrEqualTo(Integer value) {
            addCriterion("adm_id <=", value, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdIn(List<Integer> values) {
            addCriterion("adm_id in", values, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdNotIn(List<Integer> values) {
            addCriterion("adm_id not in", values, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdBetween(Integer value1, Integer value2) {
            addCriterion("adm_id between", value1, value2, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmIdNotBetween(Integer value1, Integer value2) {
            addCriterion("adm_id not between", value1, value2, "admId");
            return (Criteria) this;
        }

        public Criteria andAdmAtIsNull() {
            addCriterion("adm_at is null");
            return (Criteria) this;
        }

        public Criteria andAdmAtIsNotNull() {
            addCriterion("adm_at is not null");
            return (Criteria) this;
        }

        public Criteria andAdmAtEqualTo(String value) {
            addCriterion("adm_at =", value, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtNotEqualTo(String value) {
            addCriterion("adm_at <>", value, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtGreaterThan(String value) {
            addCriterion("adm_at >", value, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtGreaterThanOrEqualTo(String value) {
            addCriterion("adm_at >=", value, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtLessThan(String value) {
            addCriterion("adm_at <", value, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtLessThanOrEqualTo(String value) {
            addCriterion("adm_at <=", value, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtLike(String value) {
            addCriterion("adm_at like", value, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtNotLike(String value) {
            addCriterion("adm_at not like", value, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtIn(List<String> values) {
            addCriterion("adm_at in", values, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtNotIn(List<String> values) {
            addCriterion("adm_at not in", values, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtBetween(String value1, String value2) {
            addCriterion("adm_at between", value1, value2, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmAtNotBetween(String value1, String value2) {
            addCriterion("adm_at not between", value1, value2, "admAt");
            return (Criteria) this;
        }

        public Criteria andAdmPdIsNull() {
            addCriterion("adm_pd is null");
            return (Criteria) this;
        }

        public Criteria andAdmPdIsNotNull() {
            addCriterion("adm_pd is not null");
            return (Criteria) this;
        }

        public Criteria andAdmPdEqualTo(String value) {
            addCriterion("adm_pd =", value, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdNotEqualTo(String value) {
            addCriterion("adm_pd <>", value, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdGreaterThan(String value) {
            addCriterion("adm_pd >", value, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdGreaterThanOrEqualTo(String value) {
            addCriterion("adm_pd >=", value, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdLessThan(String value) {
            addCriterion("adm_pd <", value, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdLessThanOrEqualTo(String value) {
            addCriterion("adm_pd <=", value, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdLike(String value) {
            addCriterion("adm_pd like", value, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdNotLike(String value) {
            addCriterion("adm_pd not like", value, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdIn(List<String> values) {
            addCriterion("adm_pd in", values, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdNotIn(List<String> values) {
            addCriterion("adm_pd not in", values, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdBetween(String value1, String value2) {
            addCriterion("adm_pd between", value1, value2, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmPdNotBetween(String value1, String value2) {
            addCriterion("adm_pd not between", value1, value2, "admPd");
            return (Criteria) this;
        }

        public Criteria andAdmSsIsNull() {
            addCriterion("adm_ss is null");
            return (Criteria) this;
        }

        public Criteria andAdmSsIsNotNull() {
            addCriterion("adm_ss is not null");
            return (Criteria) this;
        }

        public Criteria andAdmSsEqualTo(Byte value) {
            addCriterion("adm_ss =", value, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsNotEqualTo(Byte value) {
            addCriterion("adm_ss <>", value, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsGreaterThan(Byte value) {
            addCriterion("adm_ss >", value, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsGreaterThanOrEqualTo(Byte value) {
            addCriterion("adm_ss >=", value, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsLessThan(Byte value) {
            addCriterion("adm_ss <", value, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsLessThanOrEqualTo(Byte value) {
            addCriterion("adm_ss <=", value, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsIn(List<Byte> values) {
            addCriterion("adm_ss in", values, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsNotIn(List<Byte> values) {
            addCriterion("adm_ss not in", values, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsBetween(Byte value1, Byte value2) {
            addCriterion("adm_ss between", value1, value2, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmSsNotBetween(Byte value1, Byte value2) {
            addCriterion("adm_ss not between", value1, value2, "admSs");
            return (Criteria) this;
        }

        public Criteria andAdmTlIsNull() {
            addCriterion("adm_tl is null");
            return (Criteria) this;
        }

        public Criteria andAdmTlIsNotNull() {
            addCriterion("adm_tl is not null");
            return (Criteria) this;
        }

        public Criteria andAdmTlEqualTo(String value) {
            addCriterion("adm_tl =", value, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlNotEqualTo(String value) {
            addCriterion("adm_tl <>", value, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlGreaterThan(String value) {
            addCriterion("adm_tl >", value, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlGreaterThanOrEqualTo(String value) {
            addCriterion("adm_tl >=", value, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlLessThan(String value) {
            addCriterion("adm_tl <", value, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlLessThanOrEqualTo(String value) {
            addCriterion("adm_tl <=", value, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlLike(String value) {
            addCriterion("adm_tl like", value, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlNotLike(String value) {
            addCriterion("adm_tl not like", value, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlIn(List<String> values) {
            addCriterion("adm_tl in", values, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlNotIn(List<String> values) {
            addCriterion("adm_tl not in", values, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlBetween(String value1, String value2) {
            addCriterion("adm_tl between", value1, value2, "admTl");
            return (Criteria) this;
        }

        public Criteria andAdmTlNotBetween(String value1, String value2) {
            addCriterion("adm_tl not between", value1, value2, "admTl");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}