package com.stupidzhe.springboot.domain;

import java.io.Serializable;

/**
 * @author 
 */
public class AdmRecord implements Serializable {
    private Integer admId;

    private String admAt;

    private String admPd;

    private Byte admSs;

    private String admTl;

    private static final long serialVersionUID = 1L;

    public Integer getAdmId() {
        return admId;
    }

    public void setAdmId(Integer admId) {
        this.admId = admId;
    }

    public String getAdmAt() {
        return admAt;
    }

    public void setAdmAt(String admAt) {
        this.admAt = admAt;
    }

    public String getAdmPd() {
        return admPd;
    }

    public void setAdmPd(String admPd) {
        this.admPd = admPd;
    }

    public Byte getAdmSs() {
        return admSs;
    }

    public void setAdmSs(Byte admSs) {
        this.admSs = admSs;
    }

    public String getAdmTl() {
        return admTl;
    }

    public void setAdmTl(String admTl) {
        this.admTl = admTl;
    }
}